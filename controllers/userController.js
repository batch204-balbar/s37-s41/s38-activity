const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth")

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// Controller for User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqbody.mobileNumber,
		// bcrypt.hashSync(<dataTobeHash>, <saltRound)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// User Authentication
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            //bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>)

            if(isPasswordCorrect) {
                
                return { access: auth.createAccessToken(result)}
            } else {

                return false
            }
        }

    })
}

// Get Profile
module.exports.getProfile = (reqBody) => {

    return User.findOne({_id: reqBody._id}).then(result => {

        if(result == null) {
            return false
        } else {

            const isUserRegistered = bcrypt.compareSync(reqBody._id, result._id);
            //bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>)

            if(isUserRegistered) {
                
                return { userDetails: auth.createAccessToken(result)}
            } else {

                return false
            }
        }

    })
}